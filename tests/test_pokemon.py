import requests
from src.pokeapi_class import PokeAPIHandler

pokeapi_handler = PokeAPIHandler()


# 1
def test_response_is_not_empty():
    response = requests.get("https://pokeapi.co/api/v2/pokemon")
    body = response.json()
    assert len(body["results"]) != 0


# 2
def test_status_code_is_200():
    response = requests.get("https://pokeapi.co/api/v2/pokemon")
    status_code = response.status_code
    assert status_code == 200


# 3
def test_numbers_of_pokemon_is_1279():
    response = requests.get("https://pokeapi.co/api/v2/pokemon")
    body = response.json()
    assert body["count"] == 1279


# 4
def test_response_time_under_1s():
    response = requests.get("https://pokeapi.co/api/v2/pokemon")
    assert response.elapsed.total_seconds() < 1
    print(response.elapsed.total_seconds())


def test_response_time_under_1s():
    response = requests.get("https://pokeapi.co/api/v2/pokemon")
    response_time_microsekundy = response.elapsed.microseconds // 1000
    assert response_time_microsekundy < 1000


# 5
def test_size_of_response_is_under_100_kB():
    response = requests.get("https://pokeapi.co/api/v2/pokemon")
    assert len(response.content) < 100000


def test_pagination():
    params = {
        "limit": 10,
        "offset": 20
    }
    response = requests.get("https://pokeapi.co/api/v2/pokemon", params=params)
    body = response.json()
    assert body["results"][0]["url"] == "https://pokeapi.co/api/v2/pokemon/21/"
    assert body["results"][-1]["url"] == "https://pokeapi.co/api/v2/pokemon/30/"
    # poprawniej assert body["results"][0]["url"] == f"https://pokeapi.co/api/v2/pokemon/{params["offset]+1}/"
    assert len(body["results"]) == 10
    # można też assert len(body["results"]) == params["limit"]

#sprawdzenie klasy

def test_pagination():
    params = {
        "limit": 10,
        "offset": 20
    }
    response = pokeapi_handler.gest_list_of_pokemon(params)
    body = response.json()
    assert body["results"][0]["url"] == "https://pokeapi.co/api/v2/pokemon/21/"
    assert body["results"][-1]["url"] == "https://pokeapi.co/api/v2/pokemon/30/"
    # poprawniej assert body["results"][0]["url"] == f"https://pokeapi.co/api/v2/pokemon/{params["offset]+1}/"
    assert len(body["results"]) == 10
    # można też assert len(body["results"]) == params["limit"]

def test_abc():
    response = pokeapi_handler.gest_list_of_pokemon()
    assert response.status_code == 200

